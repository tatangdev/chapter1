
-- ddl membuat table users
CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    email varchar(50) NOT NULL,
    password varchar(50) NOT NULL
);

--dml insert data ke table users
insert into users (email,password) values ('rendi@gmail.com','rendi123');

--dml untuk melihat table users ketika sudah di inputkan data
SELECT * FROM users;

--ddl membuat table accounts
CREATE TABLE accounts (
    id BIGSERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    name varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
    phone_number varchar(50)NOT NULL,
    balance INTEGER NOT NULL,
    booking_id varchar(50) NOT NULL
);

--dml insert data ke table accounts
insert into accounts (user_id,name,address,phone_number,balance) values (1,'rendi','jakarta','0123455',1000000);

--dml untuk melihat table accounts ketika sudah di inputkan data
SELECT * FROM accounts;

--ddl membuat table tickets
CREATE TABLE tickets (
    id BIGSERIAL PRIMARY KEY,
    account_id INTEGER NOT NULL,
    origin varchar(50) NOT NULL,
    destination varchar(50) NOT NULL,
    departure_date date NOT NULL,
    total_passenger INTEGER NOT NULL,
    price INTEGER NOT NULL,
    train varchar(50) NOT NULL,
    qty_stock INTEGER NOT NULL
);

--dml insert data ke table tickets
insert into tickets (account_id,origin,destination,departure_date,total_passenger,price,train,qty_stock) values (1,'jakarta','bandung','15/09/2022',1,250000,'Argo Bromo',5);

--dml untuk melihat table tickets ketika sudah di inputkan data
SELECT * FROM tickets;

--ddl membuat table bookings
CREATE TABLE bookings (
    id BIGSERIAL PRIMARY KEY,
    ticket_id INTEGER NOT NULL,
    account_id INTEGER NOT NULL,
    qty INTEGER NOT NULL
);

--dml insert data ke table bookings
insert into bookings (ticket_id,account_id,qty) values (1,1,1);

--dml untuk melihat table bookings ketika sudah di inputkan data
SELECT * FROM bookings;

--ketika sudah selesai booking akan di update data qty stock pada table tickets yaitu dikurangi jumlah qty pada table bookings
update tickets set qty_stock = qty_stock - qty where id = ticket_id;

--dam update data balance pada table accounts
update accounts set balance = balance - 250000 where id = id;

--karena ini bukan sebuah stored procedure maka querynya 
update tickets set qty_stock = qty_stock - 1 where id = 1;

--dan mengupdate data  booking_id pada table accounts
update accounts set booking_id = 1 where id = 1;

--ketika sudah selesai melakukan perjalanan dalam waktu 24 jam data pada table booking akan di hapus
delete from bookings where id = 1;

--dan akan mengupdate data qty stock pada table tickets 
update tickets set qty_stock = qty_stock + 1 where id = 1;

--dan mengupdate data booking_id pada table accounts
update accounts set booking_id = 0 where id = 1;

--menampilkan data nama,asal,tujuan,tanggal keberangkatan, total penumpang, booking id, nama kereta
select 
accounts.id,
 accounts.booking_id,
  accounts.name as nama,
   tickets.origin as asal,
    tickets.destination as tujuan,
     tickets.departure_date as tanggal_keberangkatan,
      tickets.total_passenger as total_penumpang,
       tickets.train as nama_kereta
       from accounts
       join tickets on accounts.id = tickets.account_id;

