const { User_Games, User_Biodata, User_History } = require('../models');
const bcrypt = require('bcrypt');
const jsonToken = require('jsonwebtoken');

const {
    JWT_TOKEN,
} = process.env;

module.exports = {
    daftar: async (req, res, next) => {
        
        try {
            const { username, email, password } = req.body;

            const sudahAda = await User_Games.findOne({ where: { email: email } });
            if (sudahAda) {
                return res.status(409).json({
                    status: false,
                    message: 'email sudah terpakai!!!'
                })
            }

            const encr = await bcrypt.hash(password, 10);
            const regis = await User_Games.create({
                username,
                email,
                password: encr
            })

            return res.status(201).json({
                status: false,
                message: 'akun berhasil dibuat',
                data: {
                    username: regis.username,
                    email: regis.email,
                    password: regis.password
                }
            })

        }catch (err) {
            next(err);
        }
    },

    masuk: async (req, res, next) => {

        try {
        const { email, password } = req.body;

        

        const usercompare = await User_Games.findOne({ where: { email: email }});
        if (!usercompare) {
            return res.status(400).json({
                status: false,
                message: 'username atau email salah!'
            })
        }

        const pass = await bcrypt.compare(password, usercompare.password);
        if (!pass) {
            return res.status(400).json({
                status: false,
                message: 'password salah!!'
            })
        }

        const payload = {
            id: usercompare.id,
            username: usercompare.username,
            email: usercompare.email,
        }

        const token = jsonToken.sign(payload, JWT_TOKEN);
        
        return res.status(200).json({
            status: 'sukses',
            message: 'berhasil masuk',
            data: {
                email: usercompare.email,
                jwt_token: token
            }
        })


        }catch (err) {
            next(err);
        }
    },

    akunSaya: (req, res, next) => {
        const user = req.user;
        
        try {
            return res.status(200).json({
                status: true,
                message: 'autentifikasi berhasil',
                data: user
            });
        }catch (err) {
            next(err);
        }
    },

    gantiPassword: async (req, res, next) => {

        try {
            const { passwordLama, passwordBaru, passwordBaru2 } = req.body;

            const usercompare = await User_Games.findOne({ 
                where: { 
                    id: req.user.id 
                }});
            if (!usercompare) {
                return res.status(400).json({
                    status: false,
                    message: 'user tidak di temukan!'
                })
            }
    
            const pass = await bcrypt.compare(passwordLama, usercompare.password);
            if (!pass) {
                return res.status(400).json({
                    status: false,
                    message: 'password salah!!'
                })
            }

            if (passwordBaru !== passwordBaru2) 
            return res.status(422).json({
                status: false,
                message: 'password 1 dan password 2 tidak sama!'
            });

            const hashedPassword = await bcrypt.hash(passwordBaru, 10);
            await usercompare.update({password: hashedPassword});

            return res.status(200).json({
                success: true,
                message: 'Password berhasil di ubah'
            });
        } catch (err) {
            res.status(500).json({
                status: false, 
                message: err.message
            });
        }
    },

    hapusUser: async (req, res, next) => {

        try {
        const { userId } = req.params;

        
        // ini akan menghapus user, histori game, dan biodata berdasarkan id
        const user = await (User_Games).findOne({ where: { id: userId } });
            if (!user) return res.status(404).json({ success: false, message: `User dengan id ${userId} tidak di temukan!` });
        
            await User_Games.destroy({ where: { id: userId }});
            await User_Biodata.destroy({where: { id_user : userId}});
            await User_History.destroy({where: { id_user : userId}});

        return res.status(200).json({
            status: 'success',
            message: 'data user telah berhasil di hapus',
            data: user
        })
    } catch (err) {
        next(err);
    }

    },

    daftarUser: async  (req, res, next) => {

        
        
        try { 
           const users = await User_Games.findAll();
   
       users.forEach(element => {
       console.log(element.get())
       });


        return res.status(200).json({
            status: 'success',
            message: 'berhasil dapat data',
            data: users
        })
    }catch (err) {
        next(err);
    }  
    },

    detilUser: async (req, res, next) => {
        try {
            const { userId } = req.params;

            const user = await User_Games.findOne({ where: { id: userId } });
                if (!user) return res.status(404).json({ success: false, message: `User dengan id ${userId} tidak ditemukan` });
    
            return res.status(200).json({
                status: 'success',
                message: 'data berhasil ditemukan',
                data: user
            })
        } catch (err) {
            next(err);
        }
    },

    isiBiodata: async (req, res, next) => {
        
        try {
            const { id_user, nama, umur, kota,  nomor_telepon, jenis_kelamin } = req.body;

            const sudahAda = await User_Biodata.findOne({ where: { id_user: req.user.id } });
            if (sudahAda) {
                return res.status(409).json({
                    status: false,
                    message: 'biodata sudah di isi'
                })
            }

            const isi = await User_Biodata.create({
                id_user: req.user.id,
                nama: req.user.username,
                umur,
                kota,
                nomor_telepon,
                jenis_kelamin
            })

            return res.status(201).json({
                status: true,
                message: 'biodata berhasil dibuat',
                data: {
                    idUser: isi.id_user,
                    nama: isi.nama,
                    umur: isi.umur,
                    kota: isi.kota,
                    Nomor_Telepon: isi.nomor_telepon,
                    Jenis_Kelamin: isi.jenis_kelamin
                }
            })

        }catch (err) {
            next(err);
        }
    },

    hapusBiodata: async (req, res, next) => {

        try {

        const user = await User_Biodata.findOne({ where: { id_user: req.user.id } });
            if (!user) return res.status(404).json({ success: false, message: `Biodata dengan id ${userId} tidak di temukan` });

         await User_Biodata.destroy({ where: { id_user: req.user.id }});

        return res.status(200).json({
            status: 'success',
            message: 'biodata sudah di hapus',
            data: user
        })
    } catch (err) {
        next(err);
    }

    },

     tampilBiodata: async (req, res, next) => {
        
        try { 
            const id = User_Biodata.id_user;
            const user = await User_Biodata.findOne({ where: { id_user: req.user.id } });
            if (!user) return res.status(404).json({ success: false, message: `Biodata dengan id ${req.user.id} tidak di temukan` });
 
 
         return res.status(200).json({
             status: 'success',
             message: 'berhasil dapat data',
             data: user
         })
     }catch (err) {
         next(err);
     }  
    },

    tampilSeluruhBiodata: async (req, res, next) => {
        try { 
            const users = await User_Biodata.findAll();
    
        users.forEach(element => {
        console.log(element.get())
        });
 
 
         return res.status(200).json({
             status: 'success',
             message: 'berhasil dapat data',
             data: users
         })
     }catch (err) {
         next(err);
     }  
    },

    updateBiodata: async (req, res, next) => {
        try {
            const { nama, kota, nomor_telepon, jenis_kelamin, umur } = req.body;

            const usercompare = await User_Biodata.findOne({ where: { id_user: req.user.id }});
            if (!usercompare) {
                return res.status(400).json({
                    status: false,
                    message: 'user tidak di temukan'
                })
            }

            const bio = await usercompare.update({
                nama,
                umur,
                kota,
                nomor_telepon,
                jenis_kelamin
            });

            return res.status(200).json({
                success: true,
                message: 'Biodata berhasil diupdate',
                data: {
                    nama: bio.nama,
                    umur: bio.umur,
                    kota: bio.kota,
                    nomor_telepon: bio.nomor_telepon,
                    jenis_kelamin: bio.jenis_kelamin
                }
            });
        } catch (err) {
            res.status(500).json({status: false, message: err.message});
        }
    },

    isiGame: async (req, res, next) => {
        try {
            const { id_user, username, nama_game, nilai } = req.body;

           
            const isi = await User_History.create({
                id_user: req.user.id,
                username: req.user.username,
                nama_game,
                nilai
            })

            return res.status(201).json({
                status: false,
                message: 'history game berhasil dibuat',
                data: {
                    idUser: isi.id_user,
                    nama: isi.username,
                    nama_game: isi.nama_game,
                    nilai: isi.nilai
                }
            })

        }catch (err) {
            next(err);
        }
    },

    tampilGameSaya: async (req, res, next) => {
        
        try { 
            const id = User_History.id_user;
            const user = await User_History.findAll({ where: { id_user: req.user.id } });
            if (!user) return res.status(404).json({ success: false, message: `Biodata dengan id ${id} tidak di temukan` });
 
 
         return res.status(200).json({
             status: 'success',
             message: 'berhasil dapat data',
             data: user
         })
     }catch (err) {
         next(err);
     }  
    },

    updateIsiGame: async (req, res, next) => {
        try {
            const { nama_game, nilai } = req.body;

            const { gameId } = req.params;

            const updateGame = await User_History.findOne({ where: { id: gameId }});
            if (!updateGame) {
                return res.status(400).json({
                    status: false,
                    message: 'game tidak di temukan'
                })
            }

            const bio = await updateGame.update({
                nama_game,
                nilai
            });

            return res.status(200).json({
                success: true,
                message: 'Histori game berhasil diupdate',
                data: {
                    nama_game: bio.nama_game,
                    nilai: bio.nilai
                }
            });
        } catch (err) {
            res.status(500).json({status: false, message: err.message});
        }
    },

    hapusGame: async (req, res, next) => {
        try {
            const { gameId } = req.params;
            const user = await User_History.findOne({ where: { id: gameId } });
                if (!user) return res.status(404).json({ success: false, message: `game dengan id ${gameId} tidak di temukan` });
    
             await User_History.destroy({ where: { id: gameId }});
    
            return res.status(200).json({
                status: 'success',
                message: 'game telah di hapus',
                data: user
            })
        } catch (err) {
            next(err);
        }
    },

    tampilSeluruhGameUser: async (req, res, next) => {
        try { 
            const users = await User_History.findAll();
    
        users.forEach(element => {
        console.log(element.get())
        });
 
 
         return res.status(200).json({
             status: 'success',
             message: 'berhasil dapat data',
             data: users
         })
     }catch (err) {
         next(err);
     }  
    },
}