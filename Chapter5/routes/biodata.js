const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle');

router.post('/akun/isibiodata', middle.masukDulu, cont.auth.isiBiodata);
router.delete('/akun/hapusBiodata', middle.masukDulu, cont.auth.hapusBiodata);
router.get('/akun/tampilBiodata', middle.masukDulu, cont.auth.tampilBiodata);
router.get('/tampilBiodataAll', cont.auth.tampilSeluruhBiodata);
router.patch('/akun/ganti-biodata', middle.masukDulu, cont.auth.updateBiodata);

module.exports = router;