const express = require('express');
const router = express.Router();
const user = require('./user');
const histori  = require('./histori');
const biodata = require('./biodata');


router.use('/auth', user);
router.use('/auth', histori);
router.use('/auth', biodata);

module.exports = router;