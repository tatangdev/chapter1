const express = require('express');
const router = express.Router();
const cont = require('../controllers');
const middle = require('../helpers/middle')


router.post('/auth/daftar', cont.auth.daftar);
router.post('/auth/masuk', cont.auth.masuk);
router.get('/auth/akun', middle.masukDulu, cont.auth.akunSaya);
router.get('/auth/user', cont.auth.daftarUser);
router.delete('/auth/hapus/:userId', cont.auth.hapusUser);
router.patch('/auth/ganti-password', middle.masukDulu, cont.auth.gantiPassword);
router.get('/auth/detil/:userId', cont.auth.detilUser);
router.post('/auth/akun/isibiodata', middle.masukDulu, cont.auth.isiBiodata);
router.delete('/auth/akun/hapusBiodata', middle.masukDulu, cont.auth.hapusBiodata);
router.get('/auth/akun/tampilBiodata', middle.masukDulu, cont.auth.tampilBiodata);
router.get('/auth/tampilBiodataAll', cont.auth.tampilSeluruhBiodata);
router.patch('/auth/akun/ganti-biodata', middle.masukDulu, cont.auth.updateBiodata);
router.post('/auth/akun/isiGame', middle.masukDulu, cont.auth.isiGame);
router.get('/auth/akun/tampilGameSaya', middle.masukDulu, cont.auth.tampilGameSaya);
router.patch('/auth/akun/ganti-game/:gameId', middle.masukDulu, cont.auth.updateIsiGame);
router.delete('/auth/akun/hapusGame/:gameId', middle.masukDulu, cont.auth.hapusGame);
router.get('/auth/tampilSeluruhGame', cont.auth.tampilSeluruhGameUser);


module.exports = router;