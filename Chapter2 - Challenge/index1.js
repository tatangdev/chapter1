const { read } = require('fs');
const { resolve } = require('path');
const { stdin, stdout } = require('process')

let readline = require('readline').createInterface({
    input : stdin,
    output : stdout
});



 input = (nilai) => new Promise(resolve => readline.question(nilai, (siswa) => resolve(siswa)));

async function nilaiSiswa() {

    let arrNilai = [];

    let ulangi = true;

    while (ulangi) {

    console.log(`\nProgram Input Data Nilai Siswa\n==============================`);
    console.log(`Pastikan anda hanya menginput nilai dari 0 sampai 100\n==============================`)
    let bil1 = await input("Input nilai 1: ")
    let bil2 = await input("Input nilai 2: ")
    let bil3 = await input("Input nilai 3: ")
    let bil4 = await input("Input nilai 4: ")
    let bil5 = await input("Input nilai 5: ")
    let bil6 = await input("Ketik q jika sudah selesai menginput: ")
    console.log("==============================")

    if (bil6 == "q") {
        // input data array
        if ((((bil1<= -1)||(bil2<= -1)||(bil3<= -1)||(bil4<= -1)||(bil5<= -1))) || (((bil1> 100)||(bil2> 100)||(bil3> 100)||(bil4> 100)||(bil5> 100)))) {
            console.log(`Nilai yang anda masukkan tidak boleh kurang dari 0 dan lebih dari 100, silahkan cek kembali nilai yang anda masukkan\n`);
        } else {
            arrNilai.push(+bil1);
            arrNilai.push(+bil2);
            arrNilai.push(+bil3);
            arrNilai.push(+bil4);
            arrNilai.push(+bil5);
        
    // menampilkan data array
    console.log(`\nDaftar nilai dari ${arrNilai.length} siswa :`, arrNilai.toString());

    // menampilkan nilai rata-rata
    let total = 0;
    for (let i = 0; i < arrNilai.length; i++) {
        total += arrNilai[i];
    }
    let rata = total / arrNilai.length;
    console.log("Nilai rata-rata :", rata.toString());

    // menampilkan nilai tertinggi dan terendah
    let maks = Math.max.apply(Math, arrNilai);
    let mins = Math.min.apply(Math, arrNilai);
    console.log("Nilai tertinggi :", maks.toString());
    console.log("Nilai terendah :", mins.toString());

    // mengurutkan data array dari terendah ke tertinggi
    let sorter = arrNilai.sort((a, b) => a - b);
    console.log("Urutan nilai dari terendah ke tertinggi :", sorter.toString());

    // menampilkan siswa yang lulus
    const filterTampung1 = arrNilai.filter( item1 => item1 >= 60 )
    console.log("Siswa lulus :", filterTampung1.length.toString(), "dan nilainya :", filterTampung1.toString());

    // menampilkan siswa yang tidak lulus
    const filterTampung2 = arrNilai.filter( item2 => item2 < 60 )
    console.log("Siswa tidak lulus :", filterTampung2.length.toString(), "dan nilainya :", filterTampung2.toString());

    // menampilkan siswa dengan nilai 90 dan 100
    const filterTampung3 = arrNilai.filter( item3 => item3 == 90 || item3 == 100 )
    console.log(`Siswa dengan nilai 90 dan 100 ada ${filterTampung3.length} siswa :`,filterTampung3.toString());
    
    console.log(`\n\t\tDibuat oleh: Achmad Fadilla\n`)

    // mengosongkan nilai array agar tidak terjadi penambahan nilai ketika di input kembali
    arrNilai = [];

}

    } else {
        console.log(`Inputan yang anda masukkan salah!!!\n`);
    }

let isUlangi = await input(`Ingin input lagi? (yes/no)`)
if (isUlangi != "yes") {
    ulangi = false;


    if (isUlangi != "no") {
    }
    console.log(`Terima kasih\n`);
    readline.close();
}

}
}
nilaiSiswa();

